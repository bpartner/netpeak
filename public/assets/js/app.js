
$('#convert').on('click', function (event) {
    event.preventDefault();
    var input = $('input[name="value"]').val();
    var rate = $('#rate').val();

    $('#value').html(input + ' USD = ');
    $('#cur').html($('#rate option:selected').text());
    $('#sum').html(input * rate);
});