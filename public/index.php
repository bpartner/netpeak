<?php

use Framework\App;
use Framework\Currency\Providers\CurrencyLayer;

require __DIR__ . '/../vendor/autoload.php';

// APP
$app        = App::make(new CurrencyLayer);
$currencies = $app->getList();
require __DIR__ . '/../sources/view/index.php';