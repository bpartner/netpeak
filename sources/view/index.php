<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>App</title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/app.css"/>
</head>
<body class="app">
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <form>
                <div class="form-group">
                    <label for="value">USD</label>
                    <input name="value" class="form-control">
                </div>
                Convert to
                <div class="form-group">
                    <select id="rate" name="currency" class="form-control">
                        <?php
                        foreach ($currencies as $key => $value) {
                            echo '<option value="' . $key . '">' . $value . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <button id="convert" class="btn btn-outline-primary btn-lg btn-block">Convert</button>
            </form>
        </div>
    </div>

        <blockquote   class="blockquote text-center mt-5">
            <p id="result"><span id="value"></span><span id="sum"></span> <span id="cur"></span></p>
        </blockquote>

</div>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/app.js"></script>
</body>
</html>