<?php
namespace Framework\Currency;

/**
 * Class Currency
 * @package Framework\Currency
 */
class Currency
{
    private $service;

    /**
     * Currency constructor.
     *
     * @param CurrencyService $service
     */
    public function __construct(CurrencyService $service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getCurrenciesList()
    {
        return $this->service->getCurrencies();
    }
}