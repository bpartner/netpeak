<?php
namespace Framework\Currency;

/**
 * Interface CurrencyService
 * @package Framework\Currency
 */
interface CurrencyService
{
    public function getCurrencies();

}