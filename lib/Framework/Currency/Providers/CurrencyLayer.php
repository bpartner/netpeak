<?php

namespace Framework\Currency\Providers;

use Framework\Currency\CurrencyService;
use Framework\Helpers\ArrayConvertor;
use GuzzleHttp\Client;

/**
 * Class CurrencyLayer
 * @package Framework\Currency\Providers
 */
class CurrencyLayer implements CurrencyService
{
    /**
     * @return array|null
     */
    public function getCurrencies()
    {
        $request = $this->requestServer();

        return $this->parseList($request);
    }

    /**
     * @return \Psr\Http\Message\StreamInterface
     */
    private function requestServer()
    {
        $client = new Client();
        $result = $client->get('http://www.apilayer.net/api/live?access_key=6160cfa9ccdd3b792b4443fa8db3cf41');

        return $result->getBody();
    }

    /**
     * @param $parse
     *
     * @return array|null
     */
    private function parseList($parse)
    {
        $list       = json_decode($parse, true);
        $currencies = $list['quotes'];

        $currencies = ArrayConvertor::make($currencies)->map(function ($currency) {
            return (string)$currency;
        })->toArray();

        $currencies = array_flip($currencies);

        $currencies = ArrayConvertor::make($currencies)->map(function ($currency) {
            return substr($currency, 3, 3);
        })->toArray();

        return $currencies;
    }
}