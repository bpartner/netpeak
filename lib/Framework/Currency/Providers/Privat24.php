<?php
namespace Framework\Currency\Providers;

use Framework\Currency\CurrencyService;

/**
 * Class Privat24 alternative rate provider
 * @package Framework\Currency\Providers
 */
class Privat24 implements CurrencyService
{
    public function getCurrencies()
    {
        return [
            '1' => 'UAH',
            '2' => 'RUB',
        ];
    }
}