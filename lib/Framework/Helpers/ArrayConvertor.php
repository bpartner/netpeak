<?php

namespace Framework\Helpers;

/**
 * Class ArrayConvertor
 * @package Framework\Helpers
 */
class ArrayConvertor
{
    protected $items;

    public function __construct($items)
    {
        $this->items = $items;
    }

    /**
     * @param $items
     *
     * @return ArrayConvertor
     */
    public static function make($items)
    {
        return new static($items);
    }

    /**
     * @param $callback
     *
     * @return ArrayConvertor
     */
    public function map($callback)
    {
        return new static(array_map($callback, $this->items));
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return $this->items;
    }
}