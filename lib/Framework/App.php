<?php

namespace Framework;

use Framework\Currency\Currency;

/**
 * Class App
 * @package Framework
 */
class App
{
    protected $provider;

    /**
     * App constructor.
     *
     * @param $provider
     */
    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param $provider
     *
     * @return App
     */
    public static function make($provider)
    {
        return new static($provider);
    }

    /**
     * @return mixed
     */
    public function getList()
    {
        $get_currencies = new Currency($this->provider);

        return $get_currencies->getCurrenciesList();
    }
}